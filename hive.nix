{
  inputs,
  pkgs,
  ...
}: let
  mkSystem = systemFiles: homeManagerFiles: tags: user: host: {
    deployment = {
      inherit tags;
      allowLocalDeployment = true;
      privilegeEscalationCommand = ["doas"];
      targetUser = user;
      targetHost = host;
    };

    imports =
      [
        inputs.home-manager.nixosModules.home-manager
        {
          home-manager = {
            extraSpecialArgs = {
              inherit inputs;
              inherit stateVersion;
              inherit user;
            };
            useGlobalPkgs = true;
            users.${user} = {
              imports = homeManagerFiles;
            };
            useUserPackages = true;
          };
        }
      ]
      ++ systemFiles;
  };
  stateVersion = "22.11";
  user = "nick";
in {
  meta = {
    nixpkgs = pkgs;
    specialArgs = {
      inherit inputs stateVersion user;
    };
  };

  desktop =
    mkSystem
    [./hosts/shared-configuration.nix ./hosts/desktop/configuration.nix]
    [./hosts/shared-home-manager.nix ./hosts/desktop/home-manager.nix]
    ["desktop"]
    user
    "10.0.0.190";

  laptop =
    mkSystem
    [./hosts/shared-configuration.nix ./hosts/laptop/configuration.nix]
    [./hosts/shared-home-manager.nix ./hosts/laptop/home-manager.nix]
    ["laptop" "remote"]
    user
    "10.0.0.7";
}

{
  services = {
    xserver = {
      enable = true;
      displayManager = {
        gdm.enable = true;
        gdm.wayland = false;
      };
      desktopManager.gnome = {
        enable = true;
      };
      layout = "us";
      libinput = {
        enable = true;
      };
    };
    gnome.games.enable = false;
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      jack.enable = true;
    };
    printing.enable = true;
    flatpak.enable = true;
    avahi.enable = true;
    avahi.openFirewall = true;
    sshd.enable = true;
  };
}

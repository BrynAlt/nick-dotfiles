{
  pkgs,
  lib,
  ...
}: {
  programs = {
    vscode = {
      enable = true;
      package = pkgs.vscode.fhs;
      extensions = with pkgs.vscode-extensions; [
        catppuccin.catppuccin-vsc
        elmtooling.elm-ls-vscode
        gruntfuggly.todo-tree
        haskell.haskell
        james-yu.latex-workshop
        jnoortheen.nix-ide
        justusadam.language-haskell
        kamadorueda.alejandra
        mattn.lisp
        mhutchie.git-graph
        mkhl.direnv
        ms-vscode.cpptools
        pkief.material-icon-theme
        redhat.vscode-yaml
        tamasfe.even-better-toml
        skellock.just
        streetsidesoftware.code-spell-checker
        xaver.clang-format
      ];
      userSettings = {
        "C_Cpp.clang_format_path" = "/nix/store/5jinmjv3rhgw97qiq62lvhppy4lsm680-clang-11.1.0/bin/clang-format";
        "[c]"."editor.defaultFormatter" = "xaver.clang-format";
        "editor.insertSpaces" = false;
        "files.autoSave" = "afterDelay";
        "git.confirmSync" = false;
        "git.enableSmartCommit" = true;
        "nix.enableLanguageServer" = true;
        "nix.serverPath" = lib.getExe pkgs.nil;
        "nix.formatterPath" = lib.getExe pkgs.alejandra;
        "window.menuBarVisibility" = "toggle";
        "workbench.colorTheme" = "Catppuccin Mocha";
        "workbench.iconTheme" = "material-icon-theme";
        "workbench.startupEditor" = "none";
      };
    };
  };
}

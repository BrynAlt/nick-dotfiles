{
  programs = {
    helix = {
      enable = true;
      languages = [
        {
          auto-format = true;
          formatter.command = "alejandra";
          language-server = {
            command = "nil";
          };
          name = "nix";
          roots = ["flake.nix" "flake.json"];
        }
      ];
      settings = {
        keys.insert = {
          j.k = "normal_mode";
        };
        theme = "catppuccin_mocha";
      };
    };
  };
}
  
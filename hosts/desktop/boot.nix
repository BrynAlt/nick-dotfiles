{pkgs, ...}: {
  boot = {
    extraModulePackages = [];

    initrd = {
      availableKernelModules = ["nvme" "ahci" "xhci_pci" "usb_storage" "usbhid" "sd_mod"];
      kernelModules = [];
    };

    kernelModules = ["kvm-amd"];

    kernelPackages = pkgs.linuxPackages_latest;

    loader = {
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot";
      };
      systemd-boot.enable = false;
      grub = {
        enable = true;
        device = "nodev";
        version = 2;
        useOSProber = true;
        efiSupport = true;
      };
    };
  };
}

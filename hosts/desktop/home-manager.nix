{
  inputs,
  pkgs,
  ...
}: {
  home = {
    packages = with pkgs; [
      # Internet
      brave
      discord
      signal-desktop
      # File Management
      synology-drive-client
      qbittorrent
      # Productivity
      ardour
      reaper
      drumgizmo
      yabridge
      lsp-plugins
      obs-studio
      obsidian
      libreoffice
      droidcam
      shotcut
      # Entertainment
      minecraft
      spotify
      lutris
      # Privacy & Security
      bitwarden
      # Misc
      ledger-live-desktop
      ffmpeg_5
      etcher
    ];
  };

  imports =
    (import ../../modules/editors)
    ++ (import ../../modules/terminal-emulators);
}

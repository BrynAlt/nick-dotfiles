{
  config,
  inputs,
  pkgs,
  ...
}: {
  imports = [
    ./boot.nix
    ./hardware-configuration.nix
    ./networking.nix
    ../shared-security.nix
    ../../modules/gaming/steam
    ../../modules/services/xserver
    ../../modules/sound
  ];

  services = {
    printing.enable = true;
    xserver = {
      videoDrivers = ["nvidia"];
      libinput = {
        mouse.accelProfile = "flat";
        touchpad.accelProfile = "flat";
      };
    };
  };
}

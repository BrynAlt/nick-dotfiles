{
  inputs,
  pkgs,
  stateVersion,
  user,
  ...
}: {
  console.useXkbConfig = true;

  i18n.defaultLocale = "en_US.UTF-8";

  imports = [];

  nix = {
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs = true
      keep-derivations = true
    '';
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 10d";
    };
    registry.nixpkgs.flake = inputs.nixpkgs;
    settings.trusted-users = ["${user}"];
  };

  nixpkgs = {
    config = {
      allowUnfree = true;
      permittedInsecurePackages = [
        "electron-18.1.0"
        "electron-12.2.3"
      ];
    };
  };

  users.users = {
    "nick" = {
      extraGroups = [
        "networkmanager"
        "vboxusers"
        "wheel"
      ];
      isNormalUser = true;
      shell = pkgs.zsh;
    };
  };

  environment = {
    gnome.excludePackages =
      (with pkgs; [
        gnome-photos
        gnome-tour
      ])
      ++ (with pkgs.gnome; [
        cheese # webcam tool
        gnome-music
        gedit # text editor
        epiphany # web browser
        geary # email reader
        evince # document viewer
        gnome-characters # unicode library
        totem # video player
        tali # poker game
        iagno # go game
        hitori # sudoku game
        atomix # puzzle game
        yelp # help viewer
        gnome-clocks
        gnome-weather
        gnome-maps
        gnome-contacts
        gnome-calendar
      ]);
  };

  virtualisation.libvirtd.enable = true;

  programs.dconf.enable = true;

  system.stateVersion = stateVersion;

  time.timeZone = "America/Winnipeg";

  # test
  environment.variables = {
    EDITOR = "hx";
    GRIM_DEFAULT_DIR = "$HOME/Pictures/screenshots/";
    WLR_NO_HARDWARE_CURSORS = "1";
  };
}

{
  pkgs,
  stateVersion,
  user,
  ...
}: {
  home = {
    inherit stateVersion;
    packages = with pkgs; [
      file
      fx
      fzf
      neofetch
      nil
      nnn
      unzip
      zip
      git
      flameshot
      vlc
      vim
      wget
      python3
      colmena
      wine
      steam-run
      gnome.gnome-tweaks
      gnomeExtensions.crypto-price-tracker
      gnomeExtensions.openweather
      gnomeExtensions.wallpaper-switcher
      gnomeExtensions.dash-to-panel
      gnomeExtensions.just-perfection
      gnomeExtensions.appindicator
      gnomeExtensions.start-overlay-in-application-view
      gnomeExtensions.keep-awake
    ];
    username = "${user}";
  };

  imports =
    (import ../modules/command-line)
    ++ (import ../modules/shells);

  programs.home-manager.enable = true;
}

{
  config,
  inputs,
  pkgs,
  ...
}: {
  imports = [
    ./boot.nix
    ./hardware-configuration.nix
    ./networking.nix
    ../shared-security.nix
    ../../modules/gaming/steam
    ../../modules/services/xserver
    ../../modules/sound
  ];

  users.users = {
    "nick" = {
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFSTzHgJP8Ng1N4e4a/y9ioZAjmN03uP22CohcFLBg3X nick@nixos"
      ];
    };
    "garnet" = {
      extraGroups = [
        "networkmanager"
        "wheel"
      ];
      isNormalUser = true;
      shell = pkgs.zsh;
    };
  };

  services = {
    printing.enable = true;
    xserver = {
      videoDrivers = ["amdgpu"];
      libinput = {
        mouse.accelProfile = "flat";
        touchpad.accelProfile = "flat";
      };
    };
  };
}

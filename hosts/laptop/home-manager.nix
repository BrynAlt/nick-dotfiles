{
  inputs,
  pkgs,
  ...
}: {
  home = {
    packages = with pkgs; [
      # Internet
      brave
      discord
      signal-desktop
      # File Management
      synology-drive-client
      qbittorrent
      # Productivity
      obsidian
      libreoffice
      # Entertainment
      spotify
      lutris
      # Privacy & Security
      bitwarden
      # Misc
      ledger-live-desktop
      etcher
    ];
    username = "nick";
  };

  imports =
    (import ../../modules/editors)
    ++ (import ../../modules/terminal-emulators);
}
